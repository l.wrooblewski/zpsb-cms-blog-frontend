import Nav from "./nav"

const Layout = ({ children, categories, seo, searchQuery }) => (
  <>
    <Nav categories={categories} searchQuery={searchQuery} />
    {children}
  </>
)

export default Layout
