import React, { useState } from "react"

export const Search = ({ defaultValue }) => {
  const [query, setQuery] = useState(defaultValue)

  return (
    <form action={`/search/${query}`} method="GET">
      <input
        className="searchInput"
        type="search"
        value={query}
        onChange={({ target }) => setQuery(target.value)}
        placeholder="Szukaj"
        minLength={3}
      />
    </form>
  )
}
