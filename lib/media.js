import { getStrapiURL } from "./api"

export function getStrapiMedia(media) {
  const { data } = media
  if (!data) {
    return ""
  }
  const { url } = media.data.attributes || ""
  const imageUrl = url.startsWith("/") ? getStrapiURL(url) : url
  return imageUrl
}
