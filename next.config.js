/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  images: {
    loader: "default",
    domains: ["localhost", "zpsb-cms-admin-panel.herokuapp.com"],
  },
}

module.exports = nextConfig
