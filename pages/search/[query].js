import React from "react"
import Articles from "../../components/articles"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import { fetchAPI } from "../../lib/api"

const SearchPage = ({ articles, categories, homepage, query }) => {
  return (
    <Layout categories={categories} searchQuery={query}>
      <Seo seo={homepage.attributes.seo} />
      <div className="uk-section">
        <div className="uk-container uk-container-large">
          <h2>Szukasz pod hasłem: {query}</h2>
          {articles.length ? (
            <Articles articles={articles} />
          ) : (
            "Niestety, nic nie znaleziono..."
          )}
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps({ params }) {
  const query = params.query
  // Run API calls in parallel
  const [articlesRes, categoriesRes, homepageRes] = await Promise.all([
    fetchAPI("/articles", { populate: "*" }),
    fetchAPI("/categories", { populate: "*" }),
    fetchAPI("/homepage", {
      populate: {
        hero: "*",
        seo: { populate: "*" },
      },
    }),
  ])

  const filteredArticles = articlesRes.data.filter(({ attributes }) =>
    attributes.title.includes(query)
  )

  return {
    props: {
      articles: filteredArticles,
      categories: categoriesRes.data,
      homepage: homepageRes.data,
      query: params.query,
    },
  }
}

export default SearchPage
